var config = require("../../shared/config");
var frameModule = require("ui/frame");
var fetchModule = require("fetch");
var Observable = require("data/observable").Observable;
var dialogsModule = require("ui/dialogs");
var http = require("http");

var LoadingIndicator = require("nativescript-loading-indicator").LoadingIndicator;
var loader = new LoadingIndicator();

function Comercio(info) {
    info = info || {};

    var viewModel = new Observable({
        codigo: info.codigo || "",
        tiendas: {},
        tipo: "nestle",
        idMaquina: 0
    });

    viewModel.getTiendas = function(tiendas) {
        viewModel.tiendas = tiendas;
        var navigationEntry = {
            moduleName: "views/comercio/seleccion/seleccion",
            context: {info: viewModel},
            animated: true
        };
        frameModule.Frame.defaultTransition = { name: "slideLeft" };
        frameModule.topmost().navigate(navigationEntry);
    };

    viewModel.setComercio = function(comercio) {
        viewModel.comercio = comercio;
        viewModel.comercioURL = config.apiUrl+'comercio/'+comercio.com_id;     
        var navigationEntry = {
            moduleName: "views/comercio/principal/principal",
            context: {info: viewModel},
            animated: true
        };
        frameModule.Frame.defaultTransition = { name: "slideLeft" };
        frameModule.topmost().navigate(navigationEntry);
    };

    viewModel.buscar = function(codigo, pageData) {
        console.log("Codigo: " + codigo);

        var options = {
          message: 'Buscando...',
          progress: 0.65,
          android: {
            indeterminate: true,
            cancelable: true,
            max: 100,
            progressNumberFormat: "%1d/%2d",
            progressPercentFormat: 0.53,
            progressStyle: 1,
            secondaryProgress: 1    
          }
        };

        loader.show(options);
        
        http.request({ 
            url: config.apiUrl + 'comercio/' + codigo + "?api_token=" + config.api_token, 
            method: "GET" 
        }).then(function (response) {
            var respuesta = JSON.parse(response.content);            
            
            loader.hide();

            if ( respuesta.length > 0 ){

                if ( respuesta > 1 ){                    
                    viewModel.getTiendas( respuesta );
                } else{
                    viewModel.setComercio( respuesta[0] );                    
                }
            }else{
                dialogsModule.alert({
                    message: "Código de comercio errado.",
                    okButtonText: "Aceptar"
                });
            }
        }, function (error) {
            
            console.log("Error: " + error);

            loader.hide();

            dialogsModule.alert({
                message: "Desafortunadamente no hemos podido encontrar el comercio.",
                okButtonText: "Aceptar"
            });
            return Promise.reject();
        });
    };    

    return viewModel;
}

function handleErrors(response) {
    if (!response.ok) {
        console.log(JSON.stringify(response));
        throw Error(response.statusText);
    }
    return response;
}

module.exports = Comercio;