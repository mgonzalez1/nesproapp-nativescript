var ReporteViewModel = require("../../../shared/view-models/reporte-view-model");

var frameModule = require("ui/frame");
var dialogsModule = require("ui/dialogs");
var Observable = require("data/observable").Observable;

var page;
var context;
var reporte;

function pageNavigatedTo(args) {
    page = args.object;
    context = page.navigationContext;
    page.bindingContext = context;
}
exports.onPageLoaded = pageNavigatedTo; 

exports.cambiarComercio = function(){
	frameModule.Frame.defaultTransition = { name: "slideRight" };
	frameModule.topmost().navigate("views/login/login");
}
/*
exports.solicitarSoporte = function(){


}
*/
exports.solicitarEstatus = function(){
	var comercio = context.info || context.comercio.info
	reporte = context.reporte || new ReporteViewModel({});
	reporte.estatus(comercio);
}