var ComercioViewModel = require("../../shared/view-models/comercio-view-model");
var dialogsModule = require("ui/dialogs");
var Observable = require("data/observable").Observable;

var page;
var comercio = new ComercioViewModel({});

var pageData = new Observable({ 
    codigo: "401289320"
});

exports.onPageLoaded = function(args) {
    page = args.object;
    page.bindingContext = pageData;
}

exports.siguiente = function() {

    codigo = pageData.get("codigo");

    if (codigo.trim() === "") {
        dialogsModule.alert({
            message: "Debes introducir un codigo de comercio.",
            okButtonText: "Aceptar"
        });
        return;
    }

	comercio.buscar(codigo, pageData);
}
