var ComercioViewModel = require("../../shared/view-models/comercio-view-model");
var frameModule = require("ui/frame");
frameModule.Frame.defaultTransition = { name: "flip" };

var dialogsModule = require("ui/dialogs");
var Observable = require("data/observable").Observable;

function onPageLoaded(args) {
    page = args.object;
    comercio.buscar(codigo);
}
exports.onPageLoaded = onPageLoaded; 
